package mappings

import groovy.json.JsonOutput
import groovy.json.JsonSlurper

//use the message context and pre-defined flowVars to map error context here
def _out = [
	  errorDetails: [
		transactionId: sessionVars.esbTransactionID,
		creationDateTime: sessionVars.creationTime,
		code: flowVars.errorCode,
		message: flowVars.errorMessage,
		requestedUrl: message.getInboundProperty('http.request.path'),
		requestedMethod: message.getInboundProperty('http.method')
	  ].findAll{it.value}
	]

JsonOutput.toJson(_out)